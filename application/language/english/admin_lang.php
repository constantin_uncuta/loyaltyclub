<?php
$lang['Admin Section Menu Label Users'] = "Users";
$lang['Admin Section Menu Label Customers'] = "Private";
$lang['Admin Section Menu Label Customers Documentation'] = "Documentation";
$lang['Admin Section Menu Label Customers Terms And Conditions'] = "Terms and Conditions";
$lang['Admin Section Menu Label Companies'] = "Company";
$lang['Admin Section Menu Label Customers List'] = "List";
$lang['Admin Section Menu Label Companies List'] = "List";
$lang['Admin Section Menu Label Domains List'] = "Domains";
$lang['Admin Section Page Domains Page Title'] = "Domains";
$lang['Admin Section Page Domains Label Search By Name'] = "Name";
$lang['Admin Section Page Domains Label Search By Order'] = "Order";
$lang['Admin Section Page Domains Label Search By Status'] = "Status";
$lang['Admin Section Domains Page Label Add Domain'] = "Add Domain";
$lang['Admin Section Domains Label Add Domain Form Title'] = "Add Domain";
$lang['Admin Section Domains Label Edit Domain Form Title'] = "Edit Domain";
$lang['Admin Section Domain Label Form Domain Name'] = "Name";
$lang['Admin Section Domain Label Form Domain Order'] = "Order";
$lang['The Domain Already Exist Error'] = "This domain is already inserted";
$lang['Admin Section Page Domains Label Domain Name'] = "Name";
$lang['Admin Section Page Domains Label Domain Order'] = "Order";
$lang['Admin Section Page Domains Label Domain Status'] = "Status";
$lang['Admin Section Edit Label Btn'] = "Edit";
$lang['Admin Section Page Title Label Customers'] = "Customers";
$lang['Admin Section Users Label User Name'] = "Name";
$lang['Admin Section Users Label User Reference'] = "Reference";
$lang['Admin Section Users Label User Email'] = "Email";
$lang['Admin Section Users Label User Phone'] = "Phone";
$lang['Admin Section Users Label User IBAN'] = "IBAN";
$lang['Admin Section Users Label User Address'] = "Address";
$lang['Admin Section Users Label User Status'] = "Status";
$lang['Admin Section Users Label User Edit'] = "Edit";
$lang['Admin Section Users Label User Edit User'] = "Edit User";
$lang['Admin Section Company Label Companies'] = "Companies";
$lang['Admin Section Company Label Company Name'] = "Name";
$lang['Admin Section Company Label Contact Name'] = "Added By";
$lang['Admin Section Company Label Company Email'] = "Email";
$lang['Admin Section Company Label Company Phone'] = "Phone";
$lang['Admin Section Company Label Company IBAN'] = "IBAN";
$lang['Admin Section Company Label Company Nr. ORC'] = "Nr. ORC";
$lang['Admin Section Company Label Company CUI'] = "CUI";
$lang['Admin Section Company Label Company Reference'] = "Reference";
$lang['Admin Section Company Label Company Address'] = "Address";
$lang['Admin Section Company Label Company Status'] = "Status";
$lang['Admin Section Company Label Company Amount'] = "Amount";
$lang['Admin Section Company Label Edit Company'] = "Edit Company";
$lang['Admin Section Page Name Documentation'] = "Documentation";
$lang['Admin Section Pages Label Page Edit'] = "Edit";
$lang['Admin Section Documentation Page Label Add Page'] = "Add Page";
$lang['Admin Section Documentation Label Title Add New Page Title'] = "Add Page";
$lang['Admin Section Page Name Terms and Conditions'] = "Terms and Conditions";
$lang['Admin Section Pages Page From Label Page Title'] = "Title";
$lang['Admin Section Pages Page From Label Page Content'] = "Content";
$lang['Admin Section Pages Page From Alert Page Content'] = "Please insert the page content";























































?>