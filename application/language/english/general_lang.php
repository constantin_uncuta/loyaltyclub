<?php
$lang['Companies'] = "Companies";
$lang['Pages'] = "Pages";
$lang['Login'] = "Login";
$lang['Log in'] = "Log in";
$lang['Remember me'] = "Remember me";
$lang['Register'] = "Register";
$lang['Become a professional'] = "Become a professional";
$lang['Bring'] = "Bring";
$lang['hope'] = "hope";
$lang['freedom'] = "freedom";
$lang['friends'] = "friends";
$lang['success'] = "success";
$lang['loyalty'] = "loyalty";
$lang['in your life'] = "in your life";
$lang['Become one of us'] = "Become one of us";
$lang['Join our community'] = "Join our community of 3000+ members and companies, to build a better future of shopings";
$lang['Start our business'] = "Start our business";
$lang['Yes'] = "Yes";
$lang['No'] = "No";
$lang['Success'] = "Success";
$lang['Coming Soon'] = "Coming Soon";
$lang['Forms Successful Saving Data'] = "The data were inserted successfully";
$lang['Forms Successful Updating Data'] = "The data were updated successfully";
$lang['Datatables Label Display'] = "Display";
$lang['Datatables Label Per Page'] = "per page";
$lang['Datatables Label No Data'] = "No data";
$lang['Datatables Label Page'] = "Page";
$lang['Datatables Label Of'] = "of";
$lang['Datatables Label No Records Available'] = "No Records Available";
$lang['Datatables Label Total Records'] = "total records";
$lang['Datatables Label Next'] = "Next";
$lang['Datatables Label Previous'] = "Previous";
$lang['Modals Label Btn Save Changes'] = "Save";
$lang['Modals Label Btn Cancel'] = "Cancel";
$lang['Delete Action'] = "Delete";
$lang['Delete Action Question'] = "Are you sure you want to perform this?";
$lang['Home Page Slider Slogan'] = "With the power of Loyalty Club, you can now focus only on your favourite shopping, while leaving the deep work on us!";
$lang['Loading Btn Text General'] = "Loading Btn Text General";
$lang['Action Edit Btn Label'] = "Edit";
$lang['Action Delete Btn Label'] = "Delete";
$lang['Action Enable Btn Label'] = "Enable";
$lang['Action Disable Btn Label'] = "Disable";
$lang['Status Disabled Label'] = "Disabled";
$lang['Status Enabled Label'] = "Enabled";
$lang['DataTable PlaceHolder Search Label'] = "Search value";
$lang['Search by'] = "Search by";
$lang['To Select Multiple Values Label Multiselect'] = "*To select multiple domains, press CTRL+click";
$lang['Contact Menu Label'] = 'Contact';
$lang['Contact Page Name'] = 'Contact';
$lang['Contact Page Please send an email at'] = 'Please send an email';










?>