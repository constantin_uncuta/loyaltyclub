<?php
$lang['User Section Menu Label Professional'] = "Professional";
$lang['User Section Menu Label Settings'] = "Settings";
$lang['User Section Menu Label Dark Mode'] = "Dark Mode";
$lang['User Section Dark Mode Modal Title'] = "Dark Mode";
$lang['User Section Dark Mode Modal Enable Title'] = "Are you sure you want to enable dark mode?";
$lang['User Section Dark Mode Modal Disable Title'] = "Are you sure you want to disable dark mode?";
$lang['User Section Menu Label Business'] = "Business";
$lang['User Section Menu Label Dashboard'] = "Dashboard";
$lang['User Section Menu Label Documentation'] = "Documentation";
$lang['User Section Menu Label Terms and Conditions'] = "Terms and Conditions";
$lang['User Section Menu Label Companies'] = "Companies";
$lang['User Section Menu Label Profile'] = "Profile";
$lang['User Section Menu Label My Network'] = "My Network";
$lang['User Section Menu Label My Network List'] = "List";
$lang['User Section Menu Label Loyalty Bank'] = "Loyalty Bank";
$lang['User Section Menu Label Contact Us'] = "Contact Us";
$lang['User Section Notification Label View All'] = "View All";
$lang['User Section Notification Label Mark All As Read'] = "Mark all as read";
$lang['User Section Notification Label Notifications'] = "Notifications";
$lang['User Section Graph Label Today'] = "Today";
$lang['User Section Graph Label This Month'] = "This month";
$lang['User Section Graph Label Network Incomes'] = "Network Incomes";
$lang['User Section Graph Label Personal Incomes'] = "Personal Incomes";
$lang['User Section Graph Label New Registered'] = "New Registered";
$lang['User Section Tasks Label New Tasks'] = "You have 4 new tasks. <a href='#'>Open Tasks</a>";
$lang['User Section Label Revenue'] = "Revenue";
$lang['User Section Label Users'] = "Users";
$lang['User Section Label All Users'] = "All users";
$lang['User Section Label Loyalty Tickets'] = "Tickets";
$lang['User Section Label Btn Add Ticket'] = "Add Ticket";
$lang['User Section Label Btn See All Tickets'] = "See all";
$lang['User Section Label Personal Shopping'] = "Personal Shopping";
$lang['User Section Label Personal Shopping of'] = "of";
$lang['User Section Label Personal Shopping Level 1'] = "Level 1";
$lang['User Section Label Personal Shopping Level 2'] = "Level 2";
$lang['User Section Label Personal Shopping Level 3'] = "Level 3";
$lang['User Section Label Subscription Your subscription will be renewed in'] = "Your subscription will be renewed in";
$lang['User Section Label Subscription Your subscription will expire on'] = "Your subscription will be renewed in";
$lang['User Section Label Subscription Upgrade to'] = "Upgrade to";
$lang['User Section Label Subscription Keep Me'] = "Keep me";
$lang['User Section Label My Network Revenue'] = "Revenue";
$lang['User Section Label My Network Qualified'] = "Qualified";
$lang['User Section Label My Network Incomes'] = "Incomes";
$lang['User Section Label My Network Users'] = "Users";
$lang['User Section Label My Network Level'] = "Level";
$lang['User Section Label My Network Client Code'] = "Client code";
$lang['User Section Label My Network Qualified'] = "Qualified";
$lang['User Section Label My Network Unqualified'] = "Unqualified";
$lang['User Section Label My Network Totals'] = "Totals";
$lang['User Section Profile Label Edit Profile Info'] = "Edit Profile";
$lang['User Section Profile Label Change Cover Photo'] = "Change cover photo";
$lang['User Section Profile Label Change Avatar Photo'] = "Update";
$lang['User Section Profile Label Full Name'] = "Full name";
$lang['User Section Profile Label Address'] = "Address";
$lang['User Section Profile Label Phone'] = "Phone";
$lang['User Section Profile Label Date of Birth'] = "Date of Birth";
$lang['User Section Profile Label IBAN Account'] = "IBAN Account";
$lang['User Section Profile Label Bank'] = "Bank";
$lang['User Section Profile Label You should have 18 years old'] = "You should have 18 years old";
$lang['User Section Profile Label Profile Settings'] = "Profile Settings";
$lang['User Section Profile Label Profile Change Password'] = "Change password";
$lang['User Section Profile Label Profile Old Password'] = "Old password";
$lang['User Section Profile Label Profile New Password'] = "New Password";
$lang['User Section Profile Label Profile Update Info'] = "Update";
$lang['User Section Profile Label Profile Your old password is wrong'] = "Your old password is wrong";
$lang['User Section Profile Label Profile Change Email'] = "Change Email";
$lang['User Section Profile Label Profile Old Email'] = "Old Email";
$lang['User Section Profile Label Profile New Email'] = "New Email";
$lang['User Section Profile Label Profile Email Already Exist'] = "The email already exist!";
$lang['User Section Profile Label Profile Please Check Email Address'] = "Please check you email address and confirm the action";
$lang['User Section Profile Label Profile To Confirm Change Of Email Content'] = "To confirm your email please click on this <a href='[changeEmailLink]'>Link</a>";
$lang['User Section Profile Label Profile To Confirm Change Of Email Title'] = "Confirm your email address";
$lang['User Section Profile Label Profile Reference'] = "Your reference is:";
$lang['User Section Menu Label My Tickets'] = "My Tickets";
$lang['User Section Tickets Page Label My Tickets'] = "My Tickets";
$lang['User Section Tickets Page Label Insert Ticket'] = "Insert Ticket";
$lang['User Section Tickets Page Label Not Validated By Trader'] = "It's not validated by the trader";
$lang['User Section Tickets Page Label Validated'] = "Validated";
$lang['User Section Tickets Page Label Serial'] = "Serial";
$lang['User Section Tickets Page Label Value'] = "Value";
$lang['User Section Tickets Page Label Discount'] = "Bonus";
$lang['User Section Tickets Page Label Invalid Serial'] = "This serial number is invalid";
$lang['User Section Tickets Page Label In Porgress To Be Validated'] = "This serial has already been submitted and it is in  pending validation.";
$lang['User Section Tickets Page Label Already Validated'] = "This serial has already been validated";
$lang['User Section Tickets Page Label Message To Wait Validation'] = "The dates has been inserted, please wait until when the ticket will be validated";

















































?>