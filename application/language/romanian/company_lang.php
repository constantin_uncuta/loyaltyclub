<?php

$lang['Company Section Menu Label Loyalty Bank'] = "Loyalty Bank";
$lang['Company Section Menu Label Generate Tickets'] = "Add Tickets";
$lang['Company Section Menu Label Settings'] = "Account Details";
$lang['Company Section Menu Label Edit Profile Info'] = "Profile";
$lang['Company Section Menu Label Edit Profile Company Details'] = "Company Details";
$lang['Company Section Menu Label Products'] = "Products";
$lang['Company Section Menu Label Products List'] = "List";

$lang['Company Section Company Details Label Edit Company Details'] = "Edit Company Details";
$lang['Company Section Company Details Label Company Name'] = "Name";
$lang['Company Section Company Details Label Contact Name'] = "Added By";
$lang['Company Section Company Details Label Company Email'] = "Email";
$lang['Company Section Company Details Label Company Phone'] = "Phone";
$lang['Company Section Company Details Label Company IBAN'] = "IBAN";
$lang['Company Section Company Details Label Company Nr. ORC'] = "Nr. ORC";
$lang['Company Section Company Details Label Company CUI'] = "CUI";
$lang['Company Section Company Details Label Company Reference'] = "Reference";
$lang['Company Section Company Details Label Company Address'] = "Address";
$lang['Company Section Company Details Label Company Bank'] = "Bank";
$lang['Company Section Company Details Label Company Website'] = "Website";
$lang['Company Section Company Details Label Company Phone'] = "Phone";
$lang['Company Section Company Details Label Company Country'] = "Country";
$lang['Company Section Company Details Label Company District'] = "District";
$lang['Company Section Company Details Label Company Locality'] = "Locality";
$lang['Company Section Company Details Label Company Street'] = "Street";
$lang['Company Section Company Details Label Company Number'] = "Number";
$lang['Company Section Company Details Label Company Postal Code'] = "Postal Code";
$lang['Company Section Company Details Label Company Main Activity Domain'] = "Main Activity Domain";
$lang['Company Section Company Details Label Company Additionals Activity Domains'] = "Additionals Activity Domains";
$lang['Company Section Company Details Label Edit Company Details Address'] = "Address Details";
$lang['Company Section Company Details Label Description'] = "Description";
$lang['Company Section Company Details Label Google Maps Url'] = "Google Maps Url";
$lang['Company Section Company Details Label Go To Location'] = "Go to location";
$lang['Company Section Company Details Label Our Offer'] = "Our Offer";
$lang['Company Section Company Details Label Reviews'] = "Reviews";
$lang['Company Description Nr. Chars Label'] = "Nr. Chars";
$lang['Company Description No Description Label'] = "No Description";
$lang['Company Section Ticket Label Section Title Tickets'] = "Tickets";
$lang['Company Section Ticket Label Section Ticket Serial'] = "Serial";
$lang['Company Section Ticket Label Section Ticket Discount'] = "Bonus";
$lang['Company Section Ticket Label Section Ticket Value'] = "Value";
$lang['Company Section Ticket Label Section Ticket Date'] = "Date";
$lang['Company Section Ticket Label Section Ticket Client ID'] = "Client ID";
$lang['Company Section Ticket Label Section Ticket Status'] = "Status";
$lang['Company Section Ticket Label Section Ticket Add Tickets'] = "Add Tickets";
$lang['Company Section Ticket Label Section Ticket Insert Nr Of Tickets'] = "Number of Tickets";
$lang['Company Section Ticket Label Section Ticket Max Limit Of Tickets On Insert'] = "You can insert max [nrOfTickests] tickets at once";
$lang['Company Section Ticket Label Section Ticket Status Not Used'] = "No used";
$lang['Company Section Ticket Label Section Ticket Status Used'] = "To be Validated";
$lang['Company Section Ticket Label Section Ticket Status Validated'] = "Validated";
$lang['Company Section Ticket Label Section Ticket Validate Ticket'] = "Validate";
$lang['Company Section Ticket Label Section Ticket Validate Ticket Question'] = "Are you sure you want to validate this ticket?";
$lang['Company Section Ticket Label Section Ticket Add Tickets'] = "Add";
$lang['Company Section Ticket Label Section Ticket Download Tickets'] = "Download";
$lang['Company Section Ticket Label Pdf Ticket Company'] = "Company";
$lang['Company Section Ticket Label Pdf Ticket Serial'] = "Serial";
$lang['Company Section Ticket Label Pdf Ticket Value'] = "Value";
$lang['Company Section Ticket Label Pdf Ticket Date of issue'] = "Date of issue";
$lang['Company Section Ticket Label Pdf Ticket Discount'] = "Bonus";
$lang['Company Section Ticket Label Pdf Ticket Check List'] = "Ticket check list";
$lang['Company Section Ticket Label Pdf Ticket Date'] = "Date";
$lang['Company Section Ticket Label Pdf Ticket Client ID'] = "Client ID";
$lang['Company Section Ticket Label Pdf Ticket Download'] = "View";
$lang['Company Section Ticket Label Pdf Ticket To Download Please Click'] = "To view the tickets please press";
$lang['Company Section Ticket Label Pdf Ticket To Download Please Click'] = "To view the tickets please press";
$lang['Companies Page Showing'] = "Showing";
$lang['Companies Page Of'] = "of";
$lang['Companies Show Companies'] = "Show companies";
$lang['Companies Per Page'] = "per page";
$lang['Companies All Categories Label'] = "All Categories";
$lang['Company Section Product Label Section Title Products'] = "Products";
$lang['Company Section Product Label Section Label Product Name'] = "Name";
$lang['Company Section Product Label Section Label Product Description'] = "Description";
$lang['Company Section Product Label Section Label Product Price'] = "Price";
$lang['Company Section Product Label Section Label Product Bonus'] = "Bonus";
$lang['Company Section Product Label Section Label Product Photos'] = "Photos";
$lang['Company Section Product Label Section Label Product No Products'] = "No Offers";
$lang['Company Section Product Label Section Label Add Product'] = "Add Product";
$lang['Company Section Product Label Section Label Edit Product'] = "Edit Product";
$lang['Company Section Product Label Btn Add Product'] = "Add Product";
$lang['Company Section Product Label Btn Delete Product Image'] = "Delete";
$lang['Company Section Product Label Show Offers'] = "Show Offers";
$lang['Company Section Product Label Pagination Offers'] = "Offers";
$lang['Company Section Product Display Products List'] = "Offers List";
$lang['Company Section Product Display Products Grid'] = "Offers Grid";















































?>