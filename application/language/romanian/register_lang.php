<?php
$lang['Register Register'] = "Register";
$lang['Register Account'] = "Account";
$lang['Register Account Info'] = "Account Info";
$lang['Register Done'] = "Done";
$lang['Register Account Type'] = "Account Type";
$lang['Register Select account ...'] = "Select account ...";
$lang['Register Account Type Private'] = "Private";
$lang['Register Account Type Company'] = "Company";
$lang['Register Private Name'] = "Name";
$lang['Register Private Email'] = "Email";
$lang['Register Placeholder Username'] = "John Smith";
$lang['Register Placeholder Email Address'] = "Email address";
$lang['Register Password'] = "Password";
$lang['Register Placeholder Password'] = "Password";
$lang['Register Confirm Password'] = "Confirm Password";
$lang['Register Placeholder Confirm Password'] = "Confirm Password";
$lang['Register Passwords didn&#39;t match'] = "Passwords didn&#39;t match";
$lang['Register Placeholder Sponsor'] = "Reference";
$lang['Register Sponsor'] = "Reference";
$lang['Registration Sponsor Info'] = "You introduct the indentification pass. If you don't have any identification pass then leave the field empty.";
$lang['Register Terms Label Private'] = "I accept the <a href='".site_url('terms-and-conditions/private')."' target='_blank'>terms and conditions</a>";
$lang['Register Terms Label Company'] = "I accept the <a href='".site_url('terms-and-conditions/company')."' target='_blank'>terms and conditions</a>";
$lang['Registration Next'] = "Next";
$lang['Registration Prev'] = "Prev";
$lang['Register Company Name'] = "Company Name";
$lang['Register Placeholder Company Name'] = "Company Name";
$lang['Register Company Organization Number'] = "Organization Number";
$lang['Register Placeholder Organization Number'] = "Organization Number";
$lang['You must complete the required fileds'] = "You must complete the required fileds";
$lang['Successful Saving'] = "Successful Saving";
$lang['The email already exist!'] = "The email already exist!";
$lang['The passwords do not match!'] = "The passwords do not match!";
$lang['Congratulations! You have been registered.'] = "Congratulations! You have been registered.";
$lang['The sponsor is invalid'] = "The reference is invalid";
$lang['Hello'] = "Hello";
$lang['You have been successfully registered on'] = "You have been successfully registered on";
$lang['Your login details are:'] = "Your login details are:";
$lang['Password:'] = "Password:";
$lang['We are waiting you on '] = "We are waiting you on ";
$lang['with many promotions and discounts.'] = "with many promotions and discounts.";
$lang['The team'] = "The team";
$lang['thanks you and wishes you a pleasant shopping experience.'] = "thanks you and wishes you a pleasant shopping experience.";
$lang['Please check your email address to confirm your account.'] = "Please check your email address to confirm your account.";
$lang['has been registered on'] = "has been registered on";
$lang['The company'] = "The company";
$lang['In the shortest time the team on'] = "In the shortest time the team on";
$lang['will contact your company to establish contractual details and will activate your account.'] = "will contact your company to establish contractual details and will activate your account.";
$lang['thanks you for your choice.'] = "thanks you for your choice.";
$lang['You have a new user in your team'] = "You have a new user in your team";
$lang['Congratulations a new user has registered in your team'] = "Congratulations a new user has registered in your team";
$lang['Please login into your account'] = "Please login into your account";
$lang['Thank you'] = "Thank you";
$lang['The new account for the company'] = "The new account for the company";
$lang['and we just registered on'] = "and we just registered on";
$lang['We are'] = "We are";
$lang['Please contact us to establish our contractual details and to activate our account.'] = "Please contact us to establish our contractual details and to activate our account.";
$lang['Account'] = "Account";
$lang['The new account for the company'] = "The new account for the company";
$lang['A new comapny just registered on'] = "A new comapny just registered on";
$lang['To see the products and services for this company please click on this'] = "To see the products and services for this company please click on this";
$lang['Your account is all set!'] = "Your account is all set!";
$lang['Now you can access to your account'] = "Now you can access to your account";
$lang['Go to Home Page'] = "Go to Home Page";
$lang['Invalid login details'] = "Invalid login details";
$lang['Register Success'] = "Success";
$lang['Login Modal Forgot Password?'] = "Forgot Password?";
$lang['Forgot Password Page Enter your email and we\'ll send you a reset link.'] = "Enter your email and we'll send you a reset link.";
$lang['Forgot Password Send reset link'] = "Send reset link";
$lang['Forgot Password Password recovery'] = "Password recovery";
$lang['Forgot Password You have accessed the recovery password form on '] = "You have accessed the recovery password form on ";
$lang['Forgot Password The new login details are:'] = "The new login details are:";
$lang['Forgot Password Username:'] = "Username:";
$lang['Forgot Password Password:'] = "Password:";
$lang['Forgot Password Please go to '] = "Please go to ";
$lang['Forgot Password and please change your password.'] = " and please change your password.";
$lang['Forgot Password We are waiting you on '] = " We are waiting you on ";
$lang['Forgot Password with many promotions and discounts.'] = "with many promotions and discounts.";
$lang['Forgot Password The team'] = "The team";
$lang['Forgot Password Please check your emai address'] = "Please check your emai address";
















?>