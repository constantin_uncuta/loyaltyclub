<div class="form-group row downloadContainer">
    <div class="col-md-12">
        <?=$this->lang->line('Company Section Ticket Label Pdf Ticket To Download Please Click')?> <a href="<?=$pdfUrl?>" download="Tickets.pdf" class="btn btn-warning download-pdf" target="_blank" tabindex="-1" role="button" aria-disabled="true"><?=$this->lang->line('Company Section Ticket Label Pdf Ticket Download')?></a>
    </div>
</div>