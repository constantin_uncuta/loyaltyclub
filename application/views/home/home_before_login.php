<!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0 overflow-hidden" id="banner">

        <div class="bg-holder overlay" style="background-image:url(<?=site_url('assets/img/generic/bg-1.jpg')?>);background-position: center bottom;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row justify-content-center align-items-center pt-8 pt-lg-10 pb-lg-9 pb-xl-0">
            <div class="col-md-11 col-lg-8 col-xl-4 pb-4 pb-xl-9 text-center text-xl-left"><a class="btn btn-outline-danger mb-4 fs--1 border-2x rounded-pill" href="#"><span class="mr-2" role="img" aria-label="Gift">🎁</span><?=$this->lang->line("Become a professional")?> (<?=$this->lang->line("Coming Soon")?>)</a>
              <h1 class="text-white font-weight-light"><?=$this->lang->line("Bring")?> <span class="typed-text font-weight-bold" data-typed-text='["<?=$this->lang->line('hope')?>","<?=$this->lang->line('freedom')?>","<?=$this->lang->line('friends')?>","<?=$this->lang->line('success')?>","<?=$this->lang->line('loyalty')?>"]'></span><br /><?=$this->lang->line('in your life')?></h1>
              <p class="lead text-white opacity-75"><?=$this->lang->line("Home Page Slider Slogan")?></p><a cbeautylass="btn btn-outline-light border-2x rounded-pill btn-lg mt-4 fs-0 py-2" href="#!" data-toggle="modal" data-target="#exampleModal"><?=$this->lang->line("Become one of us")?><span class="fas fa-play" data-fa-transform="shrink-6 down-1 right-5"></span></a>
             
            </div>
            <div class="col-xl-7 offset-xl-1 pb-4">
            <!-- div class="player" data-plyr-provider="youtube" data-plyr-embed-id="64zROWOR0_8"></div -->
            <div class="plyr__video-embed" id="player">
                <iframe
                  src="https://www.youtube.com/embed/64zROWOR0_8"
                  allowfullscreen
                  allowtransparency
                  allow="autoplay"
                ></iframe>
            </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section style="display:none;">

        <div class="container">
          <div class="row justify-content-center text-center">
            <div class="col-lg-8 col-xl-7 col-xxl-6">
              <h1 class="fs-2 fs-sm-4 fs-md-5">WebApp theme of the future</h1>
              <p class="lead">Built on top of Bootstrap 4, super modular Falcon provides you gorgeous design &amp; streamlined UX for your WebApp.</p>
            </div>
          </div>
          <div class="row align-items-center justify-content-center mt-8">
            <div class="col-md col-lg-5 col-xl-4 pl-lg-6"><img class="img-fluid px-6 px-md-0" src="<?=site_url("assets/img/illustrations/6.png")?>" alt="" /></div>
            <div class="col-md col-lg-5 col-xl-4 mt-4 mt-md-0">
              <h5 class="text-danger"><span class="far fa-lightbulb mr-2"></span>PLAN</h5>
              <h3>Blueprint & design </h3>
              <p>With Falcon as your guide, now you have a fine-tuned state of the earth tool to make your wireframe a reality.</p>
            </div>
          </div>
          <div class="row align-items-center justify-content-center mt-7">
            <div class="col-md col-lg-5 col-xl-4 pr-lg-6 order-md-2"><img class="img-fluid px-6 px-md-0" src="<?=site_url("assets/img/illustrations/5.png")?>" alt="" /></div>
            <div class="col-md col-lg-5 col-xl-4 mt-4 mt-md-0">
              <h5 class="text-info"> <span class="far fa-object-ungroup mr-2"></span>BUILD</h5>
              <h3>38 Sets of components</h3>
              <p>Build any UI effortlessly with Falcon's robust set of layouts, 38 sets of built-in elements, carefully chosen colors, typography, and css helpers.</p>
            </div>
          </div>
          <div class="row align-items-center justify-content-center mt-7">
            <div class="col-md col-lg-5 col-xl-4 pl-lg-6"><img class="img-fluid px-6 px-md-0" src="<?=site_url("assets/img/illustrations/4.png")?>" alt="" /></div>
            <div class="col-md col-lg-5 col-xl-4 mt-4 mt-md-0">
              <h5 class="text-success"><span class="far fa-paper-plane mr-2"></span>DEPLOY</h5>
              <h3>Review and test</h3>
              <p>From IE to iOS, rigorously tested and optimized Falcon will give the near perfect finishing to your webapp; from the landing page to the logout screen.</p>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-light text-center" style="display:none;">

        <div class="container">
          <div class="row">
            <div class="col">
              <h1 class="fs-2 fs-sm-4 fs-md-5">Here's what's in it for you</h1>
              <p class="lead">Things you will get right out of the box with Falcon.</p>
            </div>
          </div>
          <div class="row mt-6">
            <div class="col-lg-4">
              <div class="card card-span h-100">
                <div class="card-span-img"><span class="fab fa-sass fs-4 text-info"></span></div>
                <div class="card-body pt-6 pb-4">
                  <h5 class="mb-2">Bootstrap 4.3.1</h5>
                  <p>Build your webapp with the world's most popular front-end component library along with Falcon's 32 sets of carefully designed elements.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mt-6 mt-lg-0">
              <div class="card card-span h-100">
                <div class="card-span-img"><span class="fab fa-node-js fs-5 text-success"></span></div>
                <div class="card-body pt-6 pb-4">
                  <h5 class="mb-2">SCSS & Javascript files</h5>
                  <p>With your purchased copy of Falcon, you will get all the uncompressed & documented SCSS and Javascript source code files.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mt-6 mt-lg-0">
              <div class="card card-span h-100">
                <div class="card-span-img"><span class="fab fa-gulp fs-6 text-danger"></span></div>
                <div class="card-body pt-6 pb-4">
                  <h5 class="mb-2">Gulp based workflow</h5>
                  <p>All the painful or time-consuming tasks in your development workflow such as compiling the SCSS or transpiring the JS are automated.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-200 text-center" style="display:none;">

        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-9 col-xl-8">
              <div class="owl-carousel owl-theme owl-theme-dark" data-options='{"margin":30,"nav":true,"autoplay":true,"autoplayHoverPause":true,"loop":true,"dots":false,"items":1}'>
                <div class="px-5 px-sm-6">
                  <p class="fs-sm-1 fs-md-2 font-italic text-dark">Falcon is the best option if you are looking for a theme built with Bootstrap. On top of that, Falcon's creators and support staff are very brilliant and attentive to users' needs.</p>
                  <p class="fs-0 text-600">- Scott Tolinski, Web Developer</p><img class="w-auto mx-auto" src="<?=site_url("assets/img/logos/google.png")?>" alt="" height="45" />
                </div>
                <div class="px-5 px-sm-6">
                  <p class="fs-sm-1 fs-md-2 font-italic text-dark">We've become fanboys! Easy to change the modular design, great dashboard UI, enterprise-class support, fast loading time. What else do you want from a Bootstrap Theme?</p>
                  <p class="fs-0 text-600">- Jeff Escalante, Developer</p><img class="w-auto mx-auto" src="<?=site_url("assets/img/logos/netflix.png")?>" alt="" height="30" />
                </div>
                <div class="px-5 px-sm-6">
                  <p class="fs-sm-1 fs-md-2 font-italic text-dark">When I first saw Falcon, I was totally blown away by the care taken in the interface. It felt like something that I'd really want to use and something I could see being a true modern replacement to the current class of Bootstrap themes.</p>
                  <p class="fs-0 text-600">- Liam Martens, Designer</p><img class="w-auto mx-auto" src="<?=site_url("assets/img/logos/paypal.png")?>" alt="" height="45" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section>

        <div class="bg-holder overlay" style="background-image:url(<?=site_url('assets/img/generic/bg-2.jpg')?>);background-position: center top;">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row justify-content-center text-center">
            <div class="col-lg-8">
              <p class="fs-3 fs-sm-4 text-white"><?=$this->lang->line('Join our community')?></p>
              <button class="btn btn-outline-light border-2x rounded-pill btn-lg mt-4 fs-0 py-2" type="button"><?=$this->lang->line('Start our business')?> (<?=$this->lang->line("Coming Soon")?>)</button>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->




      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-dark pt-8 pb-4" style="display:none;">

        <div class="container">
          <div class="position-absolute btn-back-to-top bg-dark"><a class="text-600" href="#banner" data-fancyscroll="data-fancyscroll"><span class="fas fa-chevron-up" data-fa-transform="rotate-45"></span></a></div>
          <div class="row">
            <div class="col-lg-4">
              <h5 class="text-uppercase text-white opacity-85 mb-3">Our Mission</h5>
              <p class="text-600">Falcon enables front end developers to build custom streamlined user interfaces in a matter of hours, while it gives backend developers all the UI elements they need to develop their web app. And it's robust design can be easily integrated with backends whether your app is based on ruby on rails, laravel, express or any other serverside system.</p>
              <div class="icon-group mt-4"><a class="icon-item bg-white text-facebook" href="#!"><span class="fab fa-facebook-f"></span></a><a class="icon-item bg-white text-twitter" href="#!"><span class="fab fa-twitter"></span></a><a class="icon-item bg-white text-google-plus" href="#!"><span class="fab fa-google-plus-g"></span></a><a class="icon-item bg-white text-linkedin" href="#!"><span class="fab fa-linkedin-in"></span></a><a class="icon-item bg-white" href="#!"><span class="fab fa-medium-m"></span></a></div>
            </div>
            <div class="col pl-lg-6 pl-xl-8">
              <div class="row mt-5 mt-lg-0">
                <div class="col-6 col-md-6">
                  <h5 class="text-uppercase text-white opacity-85 mb-3">Company</h5>
                  <ul class="list-unstyled">
                    <li class="mb-1"><a class="text-600" href="#!">About</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Contact</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Careers</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Blog</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Terms</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Privacy</a></li>
                    <li><a class="text-600" href="#!">Imprint</a></li>
                  </ul>
                </div>
                <div class="col-6 col-md-6">
                  <h5 class="text-uppercase text-white opacity-85 mb-3">Product</h5>
                  <ul class="list-unstyled">
                    <li class="mb-1"><a class="text-600" href="#!">Features</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Roadmap</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Changelog</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Pricing</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Docs</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">System Status</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Agencies</a></li>
                    <li class="mb-1"><a class="text-600" href="#!">Enterprise</a></li>
                  </ul>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->