</div>
    </div>
    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->
    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script>
      
      /*-----------------------------------------------
      |   Theme Configuration
      -----------------------------------------------*/
      var storage = {
        isDark: <?=($this->darkMode ? 1 : 0)?>
      };
      var url = "<?=site_url('/')?>"
    </script>
    <script src="<?=site_url("assets/js/jquery.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/jquery-validation/jquery.validate.min.js")?>"></script>
    <script src="<?=site_url("assets/js/jquery.form.js")?>"></script>
    <script src="<?=site_url("assets/js/popper.min.js")?>"></script>
    <script src="<?=site_url("assets/js/bootstrap.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/@fortawesome/all.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/stickyfilljs/stickyfill.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/sticky-kit/sticky-kit.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/is_js/is.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/lodash/lodash.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/perfect-scrollbar/perfect-scrollbar.js")?>"></script>
    <link href="fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:100,200,300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
    <script src="<?=site_url("assets/lib/chart.js/Chart.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/datatables/js/jquery.dataTables.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/datatables-bs4/dataTables.bootstrap4.min.js")?>"></script>
    <script src="<?=site_url("assets/js/config.navbar-vertical.js")?>"></script>
    <script src="<?=site_url("assets/lib/prismjs/prism.js")?>"></script>
    <script src="<?=site_url("assets/lib/dropzone/dropzone.min.js")?>"></script>
    <script src="<?=site_url("assets/lib/datatables.net-responsive/dataTables.responsive.js")?>"></script>
    <script src="<?=site_url("assets/lib/datatables.net-responsive-bs4/responsive.bootstrap4.js")?>"></script>
    <script src="<?=site_url("assets/js/theme.js?v=".$this->config->item('versionJS')."")?>"></script>
    <script src="<?=site_url("assets/js/bootbox.all.min.js")?>"></script>
    <script src="<?=site_url("assets/js/app.js?v=".$this->config->item('versionJS')."")?>"></script>
    <?php if(isset($jsFiles)) { ?>
      <?php foreach ($jsFiles as $file) { ?>
       <?php echo '<script src="'.site_url("assets/js/$file").'?v='.$this->config->item("versionJS").'"></script>' ?>
     <?php } ?>
    <?php } ?>
    <?php if(isset($jslibs)) { ?>
      <?php foreach ($jslibs as $file) { ?>
       <?php echo '<script src="'.site_url("$file").'"></script>' ?>
     <?php } ?>
    <?php } ?>
  </body>

</html>