<div class="row">
    <?php foreach ($allCompanies as $companyDetails) { ?>
    <?php 
    $isThisCompanyNew = false;
    $companyCreatedDate = date("Y-m-d", strtotime($companyDetails['data']));  
    $FirstDay = date("Y-m-d", strtotime('monday this week'));  
    $LastDay = date("Y-m-d", strtotime('sunday this week'));  
    if($companyCreatedDate >= $FirstDay && $companyCreatedDate <= $LastDay) {
        $isThisCompanyNew = true;
    }
    ?>
    <div class="mb-4 p-2 col-md-6 col-lg-4">
        <div class="border rounded h-100 d-flex flex-column justify-content-between pb-2">
            <div class="overflow-hidden">
                <?php $companyUrl = $companyDetails['id_firma']."-".preg_replace('/[\s,\']+/', '-', $companyDetails['nume_firma']); ?>
                <div class="position-relative rounded-top overflow-hidden"><a class="d-block" href="<?=site_url('/company/'.urlencode(strtolower($companyUrl)).'')?>">
                <?php $imgUrl =  ($companyDetails['logo'] ? site_url("uploads/companies/".$companyDetails['id_firma']."/avatar-image/".$companyDetails['logo']."") : site_url("assets/img/products/noimage2.png") );?>
              
                <div style="background-image: url(<?=$imgUrl?>);height: 185px;background-size:contain;background-position: center top;background-repeat: no-repeat;"></div>
              
                </div>
                <div class="pt-3 pl-3 pr-3 pb-0">
                    <h5 class="fs-0"><a class="text-dark" href="../e-commerce/product-details.html"><?=$companyDetails['nume_firma']?></a></h5>
                    <p class="fs--1 mb-2"><a class="text-500" href="#!"><?=ucfirst(strtolower($companyDetails['mainActivity']))?></a></p>
                </div>
            </div>
            <div class="d-flex align-items-center justify-content-between px-3">
                <div> <span class="fa fa-star text-300"></span><span class="fa fa-star text-300"></span> <span class="fa fa-star text-300"></span> <span class="fa fa-star text-300"></span><span class="fa fa-star text-300"></span> <span class="ml-1">(0)</span></div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>