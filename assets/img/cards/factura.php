<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #333;
}
-->
</style></head>

<body>
<table width="1121" border="1" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="562" height="204" align="center" valign="bottom"><table width="525" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="525" height="35"><strong><em>Factura nr: </em></strong></td>
        <td width="525" align="left" valign="middle"><em><strong>Data: </strong></em></td>
      </tr>
      <tr>
        <td height="16">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
      <table width="525" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="146" align="right" valign="middle" bgcolor="#EBEBEB"><strong>Furnizor: </strong></td>
          <td width="365" align="left" valign="middle" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>Nr. reg. ORC: </strong></td>
          <td align="left" valign="middle" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>CIF:</strong></td>
          <td align="left" valign="middle" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>Adresa: </strong></td>
          <td align="left" valign="middle" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>Cod IBAN:</strong></td>
          <td align="left" valign="middle" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>Banca:</strong></td>
          <td align="left" valign="middle" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
      </table></td>
    <td width="553" align="center" valign="bottom"><table width="525" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="146" align="right" valign="middle" bgcolor="#D3D3D3"><strong>Beneficiar:</strong></td>
        <td width="365" align="left" valign="middle" bgcolor="#D3D3D3">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="middle" bgcolor="#D3D3D3"><strong>Nr. reg. ORC: </strong></td>
        <td align="left" valign="middle" bgcolor="#D3D3D3">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="middle" bgcolor="#D3D3D3"><strong>CIF:</strong></td>
        <td align="left" valign="middle" bgcolor="#D3D3D3">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="middle" bgcolor="#D3D3D3"><strong>Adresa: </strong></td>
        <td align="left" valign="middle" bgcolor="#D3D3D3">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="middle" bgcolor="#D3D3D3"><strong>Cod IBAN:</strong></td>
        <td align="left" valign="middle" bgcolor="#D3D3D3">&nbsp;</td>
      </tr>
      <tr>
        <td align="right" valign="middle" bgcolor="#D3D3D3"><strong>Banca:</strong></td>
        <td align="left" valign="middle" bgcolor="#D3D3D3">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="1121" border="1" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="204" align="center" valign="top"><br />
      <table width="1042" border="1" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="59" height="18" align="center" valign="middle">Nr. CRT</td>
        <td width="309" align="center" valign="middle">Denumire produse / servicii</td>
        <td width="43" align="center" valign="middle">U.M.</td>
        <td width="110" align="center" valign="middle">Cantitate </td>
        <td width="195" align="center" valign="middle">Pret unitar (RON)</td>
        <td width="164" align="center" valign="middle">Valoare (RON)</td>
        <td width="146" align="center" valign="middle">Valoare TVA (RON)</td>
      </tr>
      <tr>
        <td height="172" align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="25" align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>Total</strong></td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="31" align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
        <td align="right" valign="middle" bgcolor="#EBEBEB"><strong>Total plata</strong></td>
        <td colspan="2" align="center" valign="top">&nbsp;</td>
        </tr>
    </table>
      <table width="1042" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="137" height="125" align="left" valign="middle"><table width="125" border="1" align="left" cellpadding="0" cellspacing="0">
            <tr>
              <td width="121" height="113" align="center" valign="middle">Semnatura si stampila<br />
furnizorului</td>
            </tr>
          </table></td>
          <td width="282" align="left" valign="top">&nbsp;</td>
          <td width="623" align="left" valign="top"><br />
            <table width="483" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="483" height="44" align="center" valign="middle">Semnatura de primire _____________________</td>
            </tr>
          </table></td>
        </tr>
    </table></td>
  </tr>
</table>
</body>
</html>